# Slack app for sff votes

Currently made for deployment on dokku/heroku.

## Environment variables

    # slack signing secret
    dokku config:set --no-restart sff-votes SIGNING_SECRET=<...>
    # incoming webhook url
    dokku config:set --no-restart sff-votes WEBHOOK_URL=<...>
    # slack OAuth access token
    dokku config:set --no-restart sff-votes ACCESS_TOKEN=<...>

Create a postgres database in dokku:
 - install postgres plugin: https://github.com/dokku/dokku-postgres
 - create database service
 - link to app with `dokku postgres:link database_service_name app_name`

`DATABASE_URL` is then set by dokku magic.