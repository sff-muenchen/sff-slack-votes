import os
from flask_sqlalchemy import SQLAlchemy
from flask import Flask, request, make_response, Response, abort, jsonify
import hmac, hashlib
import random
from threading import Thread
import requests
import json
import datetime

basedir = os.path.abspath(os.path.dirname(__file__))


app = Flask(__name__)

db_url = os.environ.get('DATABASE_URL')
print("database: " + db_url)

app.config["SQLALCHEMY_ECHO"] = True
app.config["SQLALCHEMY_DATABASE_URI"] = db_url
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False

db = SQLAlchemy(app)
from models import Poll, VoteType, Vote
import queries
import init_db
init_db.init()

signing_secret = os.environ.get('SIGNING_SECRET')
access_token = os.environ.get('ACCESS_TOKEN')
webhook = os.environ.get('WEBHOOK_URL')

new_vote_dialog = {
    "callback_id": "",
    "title": "Create new vote",
    "submit_label": "Create",
    "notify_on_cancel": True,
    "elements": [
        {
            "type": "text",
            "label": "Title",
            "name": "vote_title"
        },
        {
            "type": "text",
            "subtype": "number",
            "value": "24",
            "label": "Duration (hours)",
            "name": "vote_duration"
        },
        {
            "type": "textarea",
            "label": "Description",
            "name": "vote_description",
            "optional": True
        }
    ]
}

def concern_dialog(poll_id, vote_type):
    return {
        "callback_id": "",
        "title": "Express concerns",
        "notify_on_cancel": True,
        "state": json.dumps({
            'poll_id': poll_id,
            'vote_type': vote_type.name
        }),
        "elements": [
            {
                "type": "textarea",
                "label": "Explanation",
                "name": "concern_explanation" ,
                "optional": False
            }
        ]
    }

active_new_vote_callback_ids = set()
active_show_vote_callback_ids = set()
active_concern_callback_ids = set()

@app.route('/')
def index():
    return make_response('<3',200)

def verify_signature(headers, data) -> bool:
    timestamp = headers.get('X-Slack-Request-Timestamp')
    signature = headers.get('X-Slack-Signature')
    req = str.encode('v0:' + str(timestamp) + ':') + data
    req_hash = 'v0=' + hmac.new(
        str.encode(signing_secret), req, hashlib.sha256
    ).hexdigest()
    return hmac.compare_digest(req_hash, signature)

@app.route('/callbacks/new_vote', methods=['POST']) 
def new_poll():
    data = request.get_data()
    if not verify_signature(request.headers, data):
        abort(401)
    callback_id = 'newpoll-' + hex(random.randint(0, 2**32))
    active_new_vote_callback_ids.add(callback_id)
    print(str(active_new_vote_callback_ids))
    thread = Thread(target=show_new_vote_dialog, kwargs={
        'callback_id': callback_id,
        'trigger_id': request.form['trigger_id']
    })
    thread.start()
    return make_response('', 200)

@app.route('/callbacks/show_votes', methods=['POST'])
def show_polls():
    data = request.get_data()
    if not verify_signature(request.headers, data):
        abort(401)
    callback_id = 'showvotes-' + hex(random.randint(0, 2**32))
    active_show_vote_callback_ids.add(callback_id)
    blocks = []
    all = request.form['text'].strip() == 'all'
    print("all: " + str(all))
    all_polls = queries.get_all_polls(not all)
    for poll in all_polls:
        voted = data.get_vote_by_user(poll.id, request.form['user_id']) is not None
        blocks.append(poll_block(poll, "View", 'view-' + str(poll.id), voted))
        blocks.append(divider)
    if len(blocks) == 0:
        blocks.append(no_votes_block)
    message = {
        'response_type': 'ephemeral',
        'blocks': blocks
    }
    print(message)
    return jsonify(message)

def poll_block(poll, button_text, button_name, voted):
    time_left = (poll.created_at + datetime.timedelta(hours=poll.duration_hours) - datetime.datetime.utcnow())
    hours = int(time_left.total_seconds() / 3600)
    minutes = int((time_left.total_seconds() - hours * 3600) / 60)
    time_left_str = f"Closes in {hours}h{minutes}m" if time_left.days >= 0 else 'Closed'
    return {
            "type": "section",
            "text": {
                "type": "mrkdwn",
                "text": f"*{poll.title}*{' (already voted)' if voted else ''}, {time_left_str}"
            },
            "accessory": {
                "type": "button",
                "text": {
                    "type": "plain_text",
                    "text": button_text,
                    "emoji": True
                },
                "value": button_name
            }
        }

no_votes_block = {
    'type': 'section',
    'text': {
        'type': 'mrkdwn',
        'text': 'There are no active votes right now.'
    }
}

divider = {
    'type': 'divider'
}

def show_new_vote_dialog(callback_id, trigger_id):
    dialog_json = dict(new_vote_dialog)
    dialog_json['callback_id'] = callback_id
    args = {
        'token': access_token,
        'trigger_id': trigger_id,
        'dialog': json.dumps(dialog_json, ensure_ascii=False)
    }
    requests.post('https://slack.com/api/dialog.open', data=args).content

@app.route('/callbacks/action', methods=['POST'])
def action_submitted():
    data = request.get_data()
    if not verify_signature(request.headers, data):
        abort(401)
    payload = json.loads(request.form['payload'])
    print('payload: ', payload)
    action_type = payload['type']
    if action_type == 'dialog_cancellation':
        callback_id = payload['callback_id']
        if callback_id in active_new_vote_callback_ids:
            active_new_vote_callback_ids.remove(callback_id)
            return make_response('', 200)
        elif callback_id in active_concern_callback_ids:
            active_concern_callback_ids.remove(callback_id)
            return make_response('', 200)
    elif action_type == 'dialog_submission':
        callback_id = payload['callback_id']
        if callback_id in active_new_vote_callback_ids:
            active_new_vote_callback_ids.remove(callback_id)
            title = payload['submission']['vote_title']
            errors = []
            if not title or not title.strip():
                errors.append({'name': 'vote_title',
                'error': 'Title can not be empty'})
            description = payload['submission']['vote_description'].rstrip()
            try:
                duration = int(payload['submission']['vote_duration'])
                if duration <= 0:
                    errors.append({'name': 'vote_duration',
                    'error': 'Duration must be a whole number greater than 0'})
            except ValueError:
                errors.append({'name': 'vote_duration',
                'error': 'Duration must be a whole number greater than 0'})
            if len(errors) != 0:
                response = {'errors': errors}
                return make_response(json.dumps(response), 200)
            poll = Poll(title=title, description=description, duration_hours=duration,
            creator_slack_user_id=payload['user']['id'])
            db.session.add(poll)
            db.session.commit()
            send_new_poll_message(poll, payload['channel'])
            return make_response('', 200)
        elif callback_id in active_concern_callback_ids:
            active_concern_callback_ids.remove(callback_id)
            try:
                explanation = payload['submission']['concern_explanation']
                state = json.loads(payload['state'])
                poll_id = state['poll_id']
                poll = queries.get_poll(poll_id)
                if poll is None:
                    return make_response('', 400)
                vote_type = VoteType[state['vote_type']]
                user_id = payload['user']['id']
                vote = Vote(poll_id=poll_id, slack_user_id=user_id, what=vote_type,
                explanation=explanation)
                success = queries.put_vote(vote)
                Thread(target=acknowlegde_vote, kwargs={
                    'response_url': payload['response_url'],
                    'success': success
                    }).start()
                return make_response('', 200)
            except (ValueError, IndexError) as e:
                print(e)
                return make_response('', 400)
            return make_response('', 400)
    elif action_type == 'block_actions':
        actions = payload['actions']
        #hier ist die id noch in value gepspeichert man kann auch state nehmen aber 
        #wusste ich hier noch nicht
        btn_view_actions = [a for a in actions if a['value'].startswith('view-')]
        if len(btn_view_actions) > 0:
            view_action = btn_view_actions[0]
            try:
                poll_id = int(view_action['value'][len('view-'):])
                poll = queries.get_poll(poll_id)
                if poll is None:
                    print('invalid poll id')
                    return make_response('', 400)
                thread = Thread(target=show_poll, kwargs = {
                    'response_url': payload['response_url'],
                    'poll': poll
                })
                thread.start()
                # show_poll(payload['response_url'], poll)
                return make_response('', 200)
            except (ValueError, IndexError) as e:
                print(e)
                return make_response('', 400)
        vote_actions = [a for a in actions if a['value'].startswith('vote-')]
        if len(vote_actions) > 0:
            print('vote clicked')
            split = vote_actions[0]['value'].split('-')
            try:
                poll_id = int(split[2])
                print('poll id:', str(poll_id))
                type_str = split[1]
                print(type_str)
                vote_type = VoteType.AGREE if type_str == 'agree' \
                else VoteType.SLIGHT_CONCERN if type_str == 'slightconcern' \
                else VoteType.SERIOUS_CONCERN if type_str == 'seriousconcern' else None
                if vote_type == VoteType.AGREE:
                    vote = Vote(poll_id=poll_id, slack_user_id=payload['user']['id'],
                    what=vote_type)
                    success = queries.put_vote(vote)
                    Thread(target=acknowlegde_vote, kwargs={
                        'response_url': payload['response_url'],
                        'success': success
                        }).start()
                    return make_response('', 200)
                elif vote_type == VoteType.SLIGHT_CONCERN or vote_type == VoteType.SERIOUS_CONCERN:
                    callback_id = 'slightconcern' if vote_type == VoteType.SERIOUS_CONCERN else 'serious-concern'
                    callback_id = callback_id + hex(random.randint(0, 2**32))
                    active_concern_callback_ids.add(callback_id)
                    Thread(target=show_concern_dialog, kwargs={
                        'trigger_id': payload['trigger_id'],
                        'callback_id': callback_id,
                        'poll_id': poll_id,
                        'vote_type': vote_type
                    }).start()
                    return make_response('', 200)

                else:
                    return make_response('', 400)
            except (ValueError, IndexError) as e:
                print(e)
                return make_response('', 400)
    return make_response('', 400)

def show_concern_dialog(callback_id, trigger_id, poll_id, vote_type):
    dialog = concern_dialog(poll_id, vote_type)
    dialog['callback_id'] = callback_id
    args = {
        'token': access_token,
        'trigger_id': trigger_id,
        'dialog': json.dumps(dialog, ensure_ascii=False)
    }
    requests.post('https://slack.com/api/dialog.open', data=args).content

def acknowlegde_vote(response_url, success):
    requests.post(response_url, json={
        'replace_original': False,
        'response_type': 'ephemeral',
        'text': 'Your vote has been cast.' if success else 'There was an error casting your vote.'
    })

def show_poll(response_url, poll):
    print("showing poll", poll)
    user_mention = f"<@{poll.creator_slack_user_id}>"
    body = {
        'response_type': 'ephemeral',
        'replace_original': False,
        'blocks': show_poll_blocks(poll, user_mention)
    } 
    print(json.dumps(body))
    print(requests.post(response_url, json=body))


def show_poll_blocks(poll, user_name):
    time_left = (poll.created_at + datetime.timedelta(hours=poll.duration_hours)) - datetime.datetime.utcnow()
    number_votes = len(queries.get_votes_for_poll(poll.id))
    if time_left.days < 0:
        time_left_str = "Closed"
        votes = queries.get_votes_for_poll(poll.id)
        agree_votes = [v for v in votes if v.what == VoteType.AGREE]
        slight_concerns = [v for v in votes if v.what == VoteType.SLIGHT_CONCERN]
        serious_concerns = [v for v in votes if v.what == VoteType.SERIOUS_CONCERN]
        blocks = [
            {
                "type": "section",
                "text": {
                    "type": "mrkdwn",
                    "text": f"*{poll.title}* by {user_name} ({time_left_str})\n{poll.description}\n{number_votes} votes"
                }
            },
            {
                "type": "section",
                "text": {
                    "type": "mrkdwn",
                    "text": f"*Agree ({len(agree_votes)})*"
                },
                "fields": [
                    {
                        'type': 'mrkdwn',
                        'text': f"<@{a.slack_user_id}>"
                    }
                    for a in agree_votes
                ] if len(agree_votes) > 0 else [{'type': 'mrkdwn', 'text': 'No agreeing votes.'}]
            },
            {
                "type": "section",
                "text": {
                    "type": "mrkdwn",
                    "text": f"*Slight concerns ({len(slight_concerns)})*"
                }
            }
        ]
        blocks.extend([
            {
                "type": "section",
                "text": {
                    "type": "mrkdwn",
                    "text": f"<@{c.slack_user_id}>: {c.explanation}"
                }
            }
            for c in slight_concerns
        ])
        blocks.append(
        {
            "type": "section",
            "text": {
                "type": "mrkdwn",
                "text": f"*Serious concerns ({len(serious_concerns)})*"
            }
        })
        blocks.extend(
        [
            {
                "type": "section",
                "text": {
                    "type": "mrkdwn",
                    "text": f"<@{c.slack_user_id}>: {c.explanation}"
                }
            }
            for c in serious_concerns
        ]
        )
        return blocks
    else:
        time_left = (poll.created_at + datetime.timedelta(hours=poll.duration_hours)) - datetime.datetime.utcnow()
        hours = int(time_left.total_seconds() / 3600)
        minutes = int((time_left.total_seconds() - hours * 3600) / 60)
        time_left_str = f"Closes in {hours}h{minutes}m"
        return [
        {
            "type": "section",
            "text": {
                        "type": "mrkdwn",
                        "text": f"*{poll.title}* by {user_name} ({time_left_str})\n{poll.description}\n{number_votes} votes"
                    }
        },
        {
                "type": "divider"
        },
        {
                "type": "actions",
                    "elements": [
                                {
                                    "type": "button",
                                    "text": {
                                        "type": "plain_text",
                                        "text": "Agree",
                                        "emoji": True
                                    },
                                    "value": "vote-agree-" + str(poll.id)
                                },
                                {
                                    "type": "button",
                                    "text": {
                                        "type": "plain_text",
                                        "text": "Slight concern",
                                        "emoji": True
                                    },
                                    "value": "vote-slightconcern-" + str(poll.id)
                                },
                                {
                                    "type": "button",
                                    "text": {
                                        "type": "plain_text",
                                        "text": "Serious concern",
                                        "emoji": True
                                    },
                                    "value": "vote-seriousconcern-" + str(poll.id)
                                }
                            ]
        }
        ]


def send_new_poll_message(poll, channel_id):
    print('sending message')
    blocks = show_poll_blocks(poll, f"<@{poll.creator_slack_user_id}>")
    blocks.insert(0, {
        'type': 'section',
        'text': {
            'type': 'mrkdwn',
            'text': f"<@{poll.creator_slack_user_id}> has a new vote for you!"
        }
    })
    requests.post(webhook, data=json.dumps({
        'channel': channel_id,
        'blocks': blocks,
        'text': f"There is a new vote for you!"
    }))

if __name__ == '__main__':
    app.run(debug=True)
