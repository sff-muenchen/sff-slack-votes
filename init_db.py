import os
from app import db, app
from models import Vote
from flask_migrate import Migrate

def init():
    # ich verstehe ehrlich gesagt nicht genau was von den beiden es jetzt braucht aber so gehts
    db.create_all()
    Migrate(app, db)