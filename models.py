from datetime import datetime
from app import db
from sqlalchemy import PrimaryKeyConstraint
import enum

class Poll(db.Model):
    __tablename__ = 'polls'
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(140), nullable=False)
    description = db.Column(db.String(1000), nullable=False)
    duration_hours = db.Column(db.Integer, nullable=False)
    created_at = db.Column(db.DateTime, default=datetime.utcnow, nullable=False)
    hide_results = db.Column(db.Boolean, default=True)
    creator_slack_user_id = db.Column(db.String, nullable=False)

    votes = db.relationship('Vote', back_populates='poll')

    def __str__(self):
        return f"{self.id} {self.title} {self.description} {self.created_at} {self.duration_hours}"

class VoteType(enum.Enum):
    AGREE = 1
    SLIGHT_CONCERN = 2
    SERIOUS_CONCERN = 3

class Vote(db.Model):
    __tablename__ = 'votes'
    __table_args__ = (PrimaryKeyConstraint('poll_id', 'slack_user_id', name='votes_pk'),)

    poll_id = db.Column(db.Integer, db.ForeignKey('polls.id'), nullable=False)
    poll = db.relationship('Poll', back_populates='votes')
    slack_user_id = db.Column(db.String(15), nullable=False)
    created_at = db.Column(db.DateTime, default=datetime.utcnow)
    what = db.Column(db.Enum(VoteType), nullable=False)
    explanation = db.Column(db.String(4000), nullable=True)

