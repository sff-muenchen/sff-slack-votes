from flask import make_response, abort
from app import db
from models import Poll, Vote
from sqlalchemy import cast
import datetime
from sqlalchemy.sql import text
import datetime

def get_all_polls(only_open=True):
    if only_open:
        # only querying for id's in sql and not whole object because the created_at column
        # somehow gets lost in the sqlalchemy mapping process 
        # war zumindest mit sqlite so vielleicht geht es jetzt mit postgres
        poll_ids = db.session.query('id').from_statement(
            text("""
            select v.id
                     from polls v where v.created_at + interval '1h' * v.duration_hours
                                         > now() at time zone 'UTC'
            """)
        ).all()
        poll_ids = set([id[0] for id in poll_ids])
        polls = db.session.query(Poll).filter(Poll.id.in_(poll_ids)).all()
    else:
        polls = Poll.query.all()
    return polls


def get_poll(poll_id: int):
    return Poll.query.filter(Poll.id == poll_id).one_or_none()

def create_poll(poll):
    db.session.add(poll)
    db.session.commit()
    db.session.refresh(poll)
    return poll

def get_vote_by_user(poll_id: int, user_id: str) -> Vote:
    return db.session.query(Vote).filter_by(poll_id=poll_id, slack_user_id=user_id).one_or_none()

def get_votes_for_poll(poll_id: int) -> [Vote]:
    return Vote.query.filter(Vote.poll_id == poll_id).all()

def put_vote(vote: Vote) -> bool:
    poll = Poll.query.filter(Poll.id == vote.poll_id).one_or_none()
    print('getting poll with id', vote.poll_id)
    if poll is None or datetime.datetime.utcnow() > (poll.created_at + datetime.timedelta(hours=poll.duration_hours)):
        print('vote for already expired poll. not counting')
        print('created at', poll.created_at, 'duration', poll.duration_hours)
        print(datetime.datetime.timestamp(poll.created_at + datetime.timedelta(hours=poll.duration_hours)))
        return False
    print('putting vote: ', vote)
    db.session.merge(vote)
    db.session.commit()
    return True